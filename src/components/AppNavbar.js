import React, { useState } from 'react';
import { FaBars, FaTimes } from 'react-icons/fa'
import { Container, Navbar, Nav } from 'react-bootstrap';
import { NavLink } from 'react-router-dom'
import './AppNavbar.css';

export default function AppNavbar() {

	const [click, setClick] = useState(false)

    const handleClick = () => setClick(!click)

    const [color,setColor] = useState(false)
    const changeColor = () => {
    	if (window.scrollY >= 250) {
    		setColor(true)
    	} else {
    		setColor(false)
    	}
    }

    window.addEventListener('scroll', changeColor)
						
    return (
    	<section className="sticky-top">
        <Navbar expand="lg" className={color ? 'navbarSection navbarSection-bg' : 'navbarSection'} >
		  	<Container id="navbar" className=" py-3 px-4">
		    <Navbar.Brand href="#homee" className="navbarBrand">ACDP</Navbar.Brand>
		    <div className={click ? 'nav-menu active' : 'nav-menu'} >
		      <Nav className="m-auto" id="nav_mn">
		        <Nav.Link href="#homee" className="navbarLink nav-item">Home</Nav.Link>
		        <Nav.Link href="#aboutMe" className="navbarLink nav-item">My Intro</Nav.Link>
		        <Nav.Link href="#skillset" className="navbarLink nav-item">Skills & Tools</Nav.Link>
		        <Nav.Link href="#qualification" className="navbarLink nav-item">Qualification</Nav.Link>
		        <Nav.Link href="#projects" className="navbarLink nav-item">Projects</Nav.Link>
		        <Nav.Link href="#contact" className="navbarLink nav-item d-lg-none d-sm-block">Contact Me</Nav.Link>
		        </Nav>
		        </div>
		       <Nav className="mr-auto">
		       	<Nav.Link href="#contact" className="navbarLink button1 py-2 px-3 d-lg-block d-none">Hire Me</Nav.Link>
		       </Nav>
		    
		<div className='hamburger' onClick={handleClick}>
                {click ? (<FaTimes size={30} style={{ color: '#f8f8f8' }} />) : (<FaBars size={30} style={{ color: '#f8f8f8' }} />)}

            </div>
		  </Container>
		</Navbar>
		</section>


    )

}