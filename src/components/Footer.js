import React, { useState } from 'react';
import { FaGitlab, FaHeart } from 'react-icons/fa'
import { Container, Row, Col } from 'react-bootstrap';
import * as Unicons from '@iconscout/react-unicons';
import './Footer.css';


export default function AppNavbar() {

	return (
		<Container fluid className="footer_sec pt-4 pb-2 mt-2">
			
			<Col>
				<p className="f_sec text-center">
					Feeling social? You can find me on these social media platforms too! 
					<a href="https://www.linkedin.com/in/arnon-christopher-de-la-paz-4b365b220/" target="_blank" className="footer_link mx-2"><Unicons.UilLinkedinAlt  size="20"/></a>
					<a href="https://github.com/arnondp" target="_blank" className="footer_link mx-2"><Unicons.UilGithub   size="20"/></a>
					<a href="https://gitlab.com/arnondelapazz" target="_blank" className="footer_link mx-2"><FaGitlab   size="20"/></a>
					<a href="https://www.facebook.com/ArnonDP" target="_blank" className="footer_link mx-2"><Unicons.UilFacebookF  size="20"/></a>
					<a href="https://www.instagram.com/arnondp/?hl=en" target="_blank" className="footer_link mx-2"><Unicons.UilInstagram  size="20"/></a>
					© 2022 | Designed & Coded by Arnon Christopher De La Paz
				</p>
			</Col>
			
		</Container>
		)

}



