import React from 'react';
import { Container, Col, Row, Image} from 'react-bootstrap';
import * as Unicons from '@iconscout/react-unicons';
import './Home.css';
import { Typewriter } from 'react-simple-typewriter'
import { FaGitlab } from 'react-icons/fa'


export default function Home() {

	return (
		<>
		<section id="homee" className="home homeSection pt-5">
		<Container fluid className="pb-0 pt-5">
		<Container>
			<Row className="pb-0 mb-0 " id="h_sec">
				<Col xs={1} className="">
				<Row className=" mt-5" id="links">
					<a href="https://www.linkedin.com/in/arnon-christopher-de-la-paz-4b365b220/" target="_blank" className="social_link  py-3"><Unicons.UilLinkedinAlt className="scl_icon"  /></a>
					<a href="https://github.com/arnondp" target="_blank" className="social_link  py-3"><Unicons.UilGithub className="scl_icon"  /></a>
					<a href="https://gitlab.com/arnondelapazz" target="_blank" className="social_link  py-3"><FaGitlab className="scl_icon"  /></a>
					<a href="https://www.facebook.com/ArnonDP" target="_blank" className="social_link  py-3"><Unicons.UilFacebookF className="scl_icon" /></a>
					<a href="https://www.instagram.com/arnondp/?hl=en" target="_blank" className="social_link  py-3"><Unicons.UilInstagram className="scl_icon" /></a>
				</Row>
				</Col>
				<Col xs={11} className="title-sec">
				<Row className="px-5" id="main_t">
					<h3 className="home1 text-center">Hi, My Name is</h3>
					<h1 className="home2 my-3 text-center">Arnon Christopher De La Paz</h1>
					<h2 className="my-3 text-center pb-3" id="home3"><span>
				          <Typewriter
				            words={['Full Stack Developer', 'Automation Developer', 'Full Stack Developer', 'Automation Developer','Full Stack Developer']}
				            loop={10}
				            cursor
				            cursorStyle='|'
				            typeSpeed={70}
				            deleteSpeed={50}
				            delaySpeed={1000}
				            
				          /> 
				        </span>
				    </h2>
				    <div className="text-center pt-3">	
					<a href="https://drive.google.com/file/d/1GeogH6PExMf87UUmxN9Mi2IPisa2xsMR/view?usp=sharing" target="_blank"className="button1 p-3 m-2">View Curriculum Vitae</a>
					<a href="#contact" className="button2 p-3 m-2">Contact Me</a>
					</div>
				</Row>
				</Col>
			</Row>
			<Col className="mouse-scroll d-flex justify-content-center pt-5" id="m_scroll">
				<a href="#aboutMe" className="mouse-scroll-icon"><Unicons.UilMouseAlt  size="50"/></a>
			</Col>
		</Container>
		</Container>
		</section>
		</>
		)

}