import React from 'react';
import { Container, Col, Row} from 'react-bootstrap';
import * as Unicons from '@iconscout/react-unicons';
import './AboutMe.css';


export default function AboutMe() {

	return (
		<>
		<Container fluid className="section about_me" id="aboutMe">
			<Container className="mt-5 pt-5" id="h_padding">
			<Row className="">
				<Col lg={5}  className="pic_m text-center">
					<img className="my_pic" alt="my picture" src="https://res.cloudinary.com/dwlw3n5zu/image/upload/v1648439361/Portfolio/unnamed_h2xpwm.jpg"></img>
				</Col>
				<Col lg={7}  className="pt-5 mt-5" id="about_ME"> 
					<h3 className="mt-2 my_intro">My Intro</h3>
					<h1 className="about_title">About Me</h1>
					<p className="about_p">
					A full stack developer that specializes in PHP-MySQL-Laravel and MERN STACK. I would likely ought to contribute in the firm by utilizing
					my web application development skills. I have been produced a number of capstone projects that
					involved both front and back end development. Along with my passion for learning and freshly acquired coding
					skills, I can able to contribute to empowering your team.
					</p>
					
					<Row>
					<Col xs={4} className="pt-2">
						<h4 className="details"><Unicons.UilUser className="icon" size="20"/> Name:</h4>
						<h4 className="details"><Unicons.UilPhoneAlt className="icon" size="20"/> Phone:</h4>
						<h4 className="details"><Unicons.UilEnvelopeAlt className="icon " size="20"/> Email:</h4>
						<h4 className="details"><Unicons.UilMapPin className="icon " size="20"/> Address:</h4>
						<h4 className="details"><Unicons.UilCalender className="icon " size="20"/> Date of birth:</h4>
					</Col>
					<Col xs={8}>
					<p className="my_details pt-1">Arnon Christopher De La Paz</p>
					<p className="my_details">+63-995-465-3972</p>
					<p className="my_details">arnondelapazz@gmail.com</p>
					<p className="my_details">City of Antipolo, Rizal-Philippines</p>
					<p className="my_details">January 25, 1995</p>
					</Col>
					<Col>
					<h4 className="my_interest-t">My Interests</h4>
					<div className="my_interest-s"> 
						<h5 className="myI"><Unicons.UilMusic className="icon" size="25"/> Music</h5>
						<h5 className="myI"><Unicons.UilFilm className="icon" size="25"/> Movie</h5>
						<h5 className="myI"><Unicons.UilTvRetro className="icon" size="25"/> TV Series</h5>
						<h5 className="myI"><Unicons.UilBasketball className="icon" size="25"/> Basketball</h5>
					</div>
					</Col>
					</Row>
					
				</Col>
			</Row>
			</Container>
		</Container>
		
		</>
		)

}