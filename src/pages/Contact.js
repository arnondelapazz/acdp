import React from 'react';
import { Container, Col, Row, Form } from 'react-bootstrap';
import * as Unicons from '@iconscout/react-unicons';
import { BiMailSend } from "react-icons/bi";
import './Contact.css';
import emailjs from "emailjs-com";
import Swal from 'sweetalert2';

export default function Projects()  {

	function sendEmail(e) {

		e.preventDefault();

	    emailjs.sendForm('service_vc00uvh','template_65vg0ti', e.target, 'FRup9Wq8u2n5i_P_Y')
	      .then((result) => {
	          console.log(result.text);

	         
	      }, (error) => {
	          console.log(error.text);
	      });
	       const Toast = Swal.mixin({
				  toast: true,
				  position: 'top-end',
				  showConfirmButton: false,
				  timer: 3000,
				  timerProgressBar: true,
				  didOpen: (toast) => {
				    toast.addEventListener('mouseenter', Swal.stopTimer)
				    toast.addEventListener('mouseleave', Swal.resumeTimer)
				  }
				})

				Toast.fire({
				  icon: 'success',
				  title: 'Email sent successfully.'
				})

	      e.target.reset()
	}

	return (

		<Container fluid className="contact_sec py-5 mt-3 c_section" id="contact">
			<Container className="px-5" id="contact_section">
				<h3 className="q_sectitle pb-0">Contact Me</h3>
				<h1 className="c_stitle mb-3">Get in Touch</h1>
				<Row>
				<Col lg={7}>
				<Form onSubmit={sendEmail}>
				<Row>
				
					<Col md={6}>
						
						  <Form.Group className="mb-3" controlId="fname">
						    <Form.Control type="text" placeholder="Name*" name="name" required/>
						  </Form.Group>
					</Col>
					<Col md={6}>
						
						  <Form.Group className="mb-3" controlId="formBasicEmail">
						    <Form.Control type="email" placeholder="Email*" name="email" required/>
						  </Form.Group>
						
					</Col>
					<Col md={12}>
						
						  <Form.Group className="mb-3" controlId="subject">
						    <Form.Control type="text" placeholder="Subject*" name="subject" required/>
						  </Form.Group>
						
					</Col>
				<Col md={12}>
				
						  <Form.Group className="mb-4" controlId="subject">
						    <textarea className="form-control" rows="5" id="message" placeholder="Message*" name="message" required></textarea>
						  </Form.Group>
					<Col className="pb-5">
						<button className="button1 py-2 px-3" type="submit">Send Email</button>
					</Col>
				
				</Col>
				
				</Row>
				</Form>
				</Col>
				<Col lg={5}>
				  <Row>
					<Col xs={2}>
					<Row>
						<span><Unicons.UilPhoneAlt className="mb-4 text-center c_icon" size="50"/></span>
						<span><Unicons.UilEnvelopeAlt className="mb-4 text-center c_icon" size="50"/></span>
						<span><Unicons.UilMapPinAlt className="mb-4 text-center c_icon" size="50"/></span>
					</Row>
					</Col>
					<Col xs={10}>
						<h3 className="c_details">Call Me</h3>
						<p className="c_details2">+63-995-465-3972</p>
						<h3 className="c_details">E-mail</h3>
						<p className="c_details2">arnondelapazz@gmail.com</p>
						<h3 className="c_details">Address</h3>
						<p className="c_details2">City of Antipolo, Rizal-Philippines</p>
					</Col>
				  </Row>
				</Col>
				
				</Row>
			</Container>
		</Container>

		)

}