import React, {useEffect} from 'react';
import { Container, Col, Row } from 'react-bootstrap';
import './Skillset.css';
import Figure from 'react-bootstrap/Figure'
import FigureImage from 'react-bootstrap/FigureImage'
import FigureCaption from 'react-bootstrap/FigureCaption'

export default function Skillset() {

	return (

  <Container className="skills_sec py-5" id="skillset" fluid>
  <Container className="py-4">
    <h3 className="text-center s_title pt-5">Skills & Tools</h3>
    <h1 className="text-center s_title2">My Toolbox</h1>
  {/*Front End*/}
    <h4 className="text-center c_title">Front End</h4>
    <Row>
    <Col xs={6} md={{ span: 2, offset: 1 }} className="d-flex justify-content-center">
      <Figure>
        <Figure.Image
          width={96}
          height={96}
          alt="html5"
          src="https://res.cloudinary.com/dwlw3n5zu/image/upload/v1648533639/Portfolio/html5_u3a4rz.png"
        />
        <Figure.Caption className="caption text-center">HTML5</Figure.Caption>
      </Figure>
    </Col>

    <Col xs={6} md={2} className="d-flex justify-content-center">
      <Figure>
        <Figure.Image
          width={96}
          height={96}
          alt="css3"
          src="https://res.cloudinary.com/dwlw3n5zu/image/upload/v1648533639/Portfolio/css3_kaqhrf.png"
        />
        <Figure.Caption className="caption  text-center">CSS3</Figure.Caption>
      </Figure>
    </Col>

    <Col xs={6} md={2} className="d-flex justify-content-center">
      <Figure>
        <Figure.Image
          width={96}
          height={96}
          alt="Javascript"
          src="https://res.cloudinary.com/dwlw3n5zu/image/upload/v1648533639/Portfolio/javascript_3_wizaqs.png"
        />
        <Figure.Caption className="caption  text-center">JavaScript</Figure.Caption>
      </Figure>
    </Col>
    
    <Col xs={6} md={2} className="d-flex justify-content-center">
      <Figure>
        <Figure.Image
          width={96}
          height={96}
          alt="Bootstrap"
          src="https://res.cloudinary.com/dwlw3n5zu/image/upload/v1648533639/Portfolio/bootstrap_3_vxmbbl.png"
        />
        <Figure.Caption className="caption  text-center">Bootstrap</Figure.Caption>
      </Figure>
    </Col>
    
    <Col sm={12} md={2} className="d-flex justify-content-center">
      <Figure>
        <Figure.Image
          width={96}
          height={96}
          alt="React"
          src="https://res.cloudinary.com/dwlw3n5zu/image/upload/v1648533639/Portfolio/react_2_datndz.png"
        />
        <Figure.Caption className="caption  text-center">React</Figure.Caption>
      </Figure>
    </Col>
		</Row>
  {/*Back End*/}
    <h4 className="text-center c_title">Back End</h4>
    <Row>
    <Col xs={6} md={2} className="d-flex justify-content-center">
      <Figure>
        <Figure.Image
          width={96}
          height={96}
          alt="PHP"
          src="https://res.cloudinary.com/dwlw3n5zu/image/upload/v1648535033/Portfolio/php_1_ewowj3.png"
        />
        <Figure.Caption className="caption  text-center">PHP</Figure.Caption>
      </Figure>
    </Col>

    <Col xs={6} md={2} className="d-flex justify-content-center">
      <Figure>
        <Figure.Image
          width={96}
          height={96}
          alt="MySQL"
          src="https://res.cloudinary.com/dwlw3n5zu/image/upload/v1648534287/Portfolio/mysql_2_nlmbnl.png"
        />
        <Figure.Caption className="caption  text-center">MySQL</Figure.Caption>
      </Figure>
    </Col>

    <Col xs={6} md={2} className="d-flex justify-content-center">
      <Figure>
        <Figure.Image
          width={96}
          height={96}
          alt="Laravel"
          src="https://res.cloudinary.com/dwlw3n5zu/image/upload/v1648534263/Portfolio/laravel_akldy8.png"
        />
        <Figure.Caption className="caption  text-center">Laravel</Figure.Caption>
      </Figure>
    </Col>

    <Col xs={6} md={2} className="d-flex justify-content-center">
      <Figure>
        <Figure.Image
          width={96}
          height={96}
          alt="mogoDB"
          src="https://res.cloudinary.com/dwlw3n5zu/image/upload/v1648534562/Portfolio/mongodb_3_xpnjk8.png"
        />
        <Figure.Caption className="caption  text-center">mongoDB</Figure.Caption>
      </Figure>
    </Col>

    <Col xs={6} md={2} className="d-flex justify-content-center">
      <Figure>
        <Figure.Image
          width={96}
          height={96}
          alt="ExpressJS"
          src="https://res.cloudinary.com/dwlw3n5zu/image/upload/v1648534827/Portfolio/png-transparent-express-js-node-js-javascript-mongodb-node-js-text-trademark-logo-removebg-preview_ar4jk7.png"
        />
        <Figure.Caption className="caption  text-center">ExpressJS</Figure.Caption>
      </Figure>
    </Col>
    
    <Col xs={6} md={2} className="d-flex justify-content-center">
      <Figure>
        <Figure.Image
          width={96}
          height={96}
          alt="NodeJS"
          src="https://res.cloudinary.com/dwlw3n5zu/image/upload/v1648534679/Portfolio/node-js_3_ezrlyy.png"
        />
        <Figure.Caption className="caption  text-center">NodeJS</Figure.Caption>
      </Figure>
    </Col>
    </Row>
  {/*RPA*/}
    <h4 className="text-center c_title">RPA(Robotic Process Automation)</h4>
    <Row>
    <Col xs={6} md={{ span: 2, offset: 4 }} className="d-flex justify-content-center">
      <Figure>
        <Figure.Image
          width={110}
          height={110}
          alt="Blue Prism"
          src="https://res.cloudinary.com/dwlw3n5zu/image/upload/v1648535923/Portfolio/236-2366819_blue-prism-rpa-logo-hd-png-download-removebg-preview_i0hyag.png"
        />
        <Figure.Caption className="caption  text-center">Blue Prism</Figure.Caption>
      </Figure>
    </Col>

    <Col xs={6} md={2} className="d-flex justify-content-center">
      <Figure>
        <Figure.Image
          width={96}
          height={96}
          alt="UiPath"
          src="https://res.cloudinary.com/dwlw3n5zu/image/upload/v1648460490/Portfolio/uipath-3_qkydir.svg"
        />
        <Figure.Caption className="caption  text-center">UiPath</Figure.Caption>
      </Figure>
    </Col>
    </Row>
  {/*Other Tools*/}
  <h4 className="text-center c_title">Other Tools</h4>
    <Row>
    <Col xs={6} md={{ span: 2, offset: 1 }} className="d-flex justify-content-center">
      <Figure>
        <Figure.Image
          width={96}
          height={96}
          alt="AWS"
          src="https://res.cloudinary.com/dwlw3n5zu/image/upload/v1648534263/Portfolio/aws_ovw0gz.png"
        />
        <Figure.Caption className="caption  text-center">AWS</Figure.Caption>
      </Figure>
    </Col>

    <Col xs={6} md={2} className="d-flex justify-content-center">
      <Figure>
        <Figure.Image
          width={96}
          height={96}
          alt="Powershell"
          src="https://res.cloudinary.com/dwlw3n5zu/image/upload/v1648534263/Portfolio/powershell_tmoilq.png"
        />
        <Figure.Caption className="caption  text-center">Powershell</Figure.Caption>
      </Figure>
    </Col>

    <Col xs={6} md={2} className="d-flex justify-content-center">
      <Figure>
        <Figure.Image
          width={96}
          height={96}
          alt="Git"
          src="https://res.cloudinary.com/dwlw3n5zu/image/upload/v1648534263/Portfolio/git_1_fowe98.png"
        />
        <Figure.Caption className="caption  text-center">Git</Figure.Caption>
      </Figure>
    </Col>

    <Col xs={6} md={2} className="d-flex justify-content-center">
      <Figure>
        <Figure.Image
          width={96}
          height={96}
          alt="Github"
          src="https://res.cloudinary.com/dwlw3n5zu/image/upload/v1648536518/Portfolio/github_2_vwtunm.png"
        />
        <Figure.Caption className="caption  text-center">Github</Figure.Caption>
      </Figure>
    </Col>

    <Col xs={6} md={2} className="d-flex justify-content-center">
      <Figure>
        <Figure.Image
          width={96}
          height={96}
          alt="GitLab"
          src="https://res.cloudinary.com/dwlw3n5zu/image/upload/v1648534262/Portfolio/gitlab_1_quder0.png"
        />
        <Figure.Caption className="caption  text-center">Gitlab</Figure.Caption>
      </Figure>
    </Col>
    
    <Col xs={6} md={{ span: 2, offset: 4 }} className="d-flex justify-content-center">
      <Figure>
        <Figure.Image
          width={96}
          height={96}
          alt="Postman"
          src="https://res.cloudinary.com/dwlw3n5zu/image/upload/v1648534263/Portfolio/postman_1_ooudu7.png"
        />
        <Figure.Caption className="caption  text-center">Postman</Figure.Caption>
      </Figure>
    </Col>

    <Col sm={12} md={2} className="d-flex justify-content-center">
      <Figure>
        <Figure.Image
          width={96}
          height={96}
          alt="Sublime Text 3"
          src="https://res.cloudinary.com/dwlw3n5zu/image/upload/v1648534263/Portfolio/sublime-text_n9ezfl.png"
        />
        <Figure.Caption className="caption  text-center">Sublime Text 3</Figure.Caption>
      </Figure>
    </Col>
    </Row>
  </Container>
  </Container>
		)

}