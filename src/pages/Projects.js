import React, {useEffect} from 'react';
import { Container, Col, Row } from 'react-bootstrap';
import * as Unicons from '@iconscout/react-unicons';
import './Projects.css';
import Figure from 'react-bootstrap/Figure'
import FigureImage from 'react-bootstrap/FigureImage'
import FigureCaption from 'react-bootstrap/FigureCaption'

export default function Projects() {

	return (
		<Container fluid className="section proj_sec" id="projects">
		<Container className="pt-5 mt-3">
			<h3 className="text-center q_sectitle">My Projects</h3>
			<h1 className="text-center q_subsectitle pb-5">Recent Works</h1>
			<Row>
				<Col lg={6} className="mb-5 pt-3 pb-5 d-flex justify-content-center order-lg-6 order-6">
			       <img src="https://res.cloudinary.com/dwlw3n5zu/image/upload/v1648629400/Portfolio/AC_EDIT_1_fpzsit.png" alt="" className="proj_pic"></img>
				</Col>
				<Col lg={6} className="mb-5 pb-3 order-lg-1 order-1">
					<h3 className="p_title pb-3">MERN E-Commerce</h3>
		      		<p className="p_desc pb-3">Generate an E-Commerce APP with the features of needes in a APP format, from log in and register, add/enable/disable a product for admin only, view active products for regular and guest users, admin dashboard, view singe product, add to cart for logged in users, added check out to create an order, and most especially hosted the application via Vercel.</p>
		      		<p className="pb-3">
		      			<span className="px-1">
		      			<Figure.Image
				          width={30}
				          height={30}
				          alt="171x180"
				          src="https://res.cloudinary.com/dwlw3n5zu/image/upload/v1648534562/Portfolio/mongodb_3_xpnjk8.png"
				        />
				        </span>
				        <span className="px-1">
				        <Figure.Image
				         width={30}
				          height={30}
				          alt="171x180"
				          src="https://res.cloudinary.com/dwlw3n5zu/image/upload/v1648534827/Portfolio/png-transparent-express-js-node-js-javascript-mongodb-node-js-text-trademark-logo-removebg-preview_ar4jk7.png"
				        />
				        </span>
				        <span className="px-1">
				        <Figure.Image
				         width={30}
				          height={30}
				          alt="171x180"
				          src="https://res.cloudinary.com/dwlw3n5zu/image/upload/v1648533639/Portfolio/react_2_datndz.png"
				        />
				        </span>
				        <span className="px-1">
				        <Figure.Image
				         width={30}
				          height={30}
				          alt="171x180"
				          src="https://res.cloudinary.com/dwlw3n5zu/image/upload/v1648534679/Portfolio/node-js_3_ezrlyy.png"
				        />
				        </span>
		      		</p>
		      		<a href="https://laraos.vercel.app/" target="_blank" className="button1 p-3 m-2">View It Here</a>
					<a href="https://gitlab.com/zui1/b123/capstone3-delapaz" target="_blank" className="button2 p-3 m-2" >View Gitlab Repo</a>
				</Col>
				<Col lg={6} className="mb-5 pb-3 order-lg-2 order-3">
			       <h3 className="p_title pb-3">E-Commerce API</h3>
		      		<p className="p_desc pb-3">Develop an E-Commerce API with a rundown of user registration and authentication, admin authorization and management, non-admin user check-out, retrieve single and active products, retrieve authenticated user’s orders, non-admin user display products per order, and lastly hosted the application via Heroku.</p>
		      		<p className="pb-3"> 
		      			<span className="px-1">
		      			<Figure.Image
				          width={30}
				          height={30}
				          alt="171x180"
				          src="https://res.cloudinary.com/dwlw3n5zu/image/upload/v1648534562/Portfolio/mongodb_3_xpnjk8.png"
				        />
				        </span>
				        <span className="px-1">
				        <Figure.Image
				         width={30}
				          height={30}
				          alt="171x180"
				          src="https://res.cloudinary.com/dwlw3n5zu/image/upload/v1648534827/Portfolio/png-transparent-express-js-node-js-javascript-mongodb-node-js-text-trademark-logo-removebg-preview_ar4jk7.png"
				        />
				        </span>
				        <span className="px-1">
				        <Figure.Image
				         width={30}
				          height={30}
				          alt="171x180"
				          src="https://res.cloudinary.com/dwlw3n5zu/image/upload/v1648534679/Portfolio/node-js_3_ezrlyy.png"
				        />
				        </span>
				        <span className="px-1">
				        <Figure.Image
				          width={30}
				          height={30}
				          alt="171x180"
				          src="https://res.cloudinary.com/dwlw3n5zu/image/upload/v1648534263/Portfolio/postman_1_ooudu7.png"
				        />
				        </span>
		      		</p>
					<a href="https://gitlab.com/zui1/b123/capstone2-delapaz" target="_blank" className="button2 p-3 m-2">View Gitlab Repo</a>
				</Col>
				<Col lg={6} className="mb-5 pt-3 pb-5 d-flex justify-content-center order-lg-3 order-2">
					<img src="https://res.cloudinary.com/dwlw3n5zu/image/upload/v1648629399/Portfolio/AC_EDIT_2_ncbjbp.png" alt="" className="proj_pic"></img>
				</Col>
				 <Col lg={6} className="mb-5 pt-3 pb-5 d-flex justify-content-center order-lg-4 order-4">
			       <img src="https://res.cloudinary.com/dwlw3n5zu/image/upload/v1648704075/Portfolio/AC_EDIT_1920_1200_px_egkf2d.png" alt="" className="proj_pic"></img>
				</Col>
				<Col lg={6} className="mb-5 pb-3 order-lg-5 order-5">
					<h3 className="p_title pb-3">Static Portfolio Website</h3>
					<p className="p_desc pb-3">Created a portfolio with a focal point of home/landing page that focusing in informing and empowering the visitor of the website. There are also a contact section where you can see my email and phone number. As you go along with the website you can see a responsive nav bar, footer, and most especially the social media icons that linked to different social media accounts.</p>
					<p className="pb-3">
						<span className="px-1">
						<Figure.Image
				          width={30}
				          height={30}
				          alt="171x180"
				          src="https://res.cloudinary.com/dwlw3n5zu/image/upload/v1648533639/Portfolio/html5_u3a4rz.png"
				        />
				        </span>
				        <span className="px-1">
						<Figure.Image
				          width={30}
				          height={30}
				          alt="171x180"
				          src="https://res.cloudinary.com/dwlw3n5zu/image/upload/v1648533639/Portfolio/css3_kaqhrf.png"
				        />
				        </span>
				        <span className="px-1">
						<Figure.Image
				          width={30}
				          height={30}
				          alt="171x180"
				          src="https://res.cloudinary.com/dwlw3n5zu/image/upload/v1648533639/Portfolio/javascript_3_wizaqs.png"
				        />
				        </span>
				        <span className="px-1">
						<Figure.Image
				          width={30}
				          height={30}
				          alt="171x180"
				          src="https://res.cloudinary.com/dwlw3n5zu/image/upload/v1648533639/Portfolio/bootstrap_3_vxmbbl.png"
				        />
				        </span>
					</p>
					<a href="https://arnondp.github.io/capstone-1-delapaz/" target="_blank" className="button1 p-3 m-2">View It Here</a>
					<a href="https://gitlab.com/zui1/b123/capstone1-delapaz" target="_blank" className="button2 p-3 m-2">View Gitlab Repo</a>
				</Col>
			</Row>
		</Container>			
		</Container>
		)
}