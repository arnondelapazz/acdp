import React, {useEffect} from 'react';
import { Container, Col, Row, Card} from 'react-bootstrap';
import * as Unicons from '@iconscout/react-unicons';
import './Qualification.css';
import { BsFillHouseDoorFill, BsCodeSlash } from "react-icons/bs";
import { FaFileCode } from 'react-icons/fa';

export default function Qualification() {

	return (

		<Container fluid className="section qualification_sec" id="qualification">
			<Container className="pb-4  pt-5 mt-1">
			<h3 className="text-center q_sectitle">Qualification</h3>
			<h1 className="text-center q_subsectitle pb-5">My Awesome Journey</h1>
				<Row className="">	
				<Col md={6}>
						<h3 className="quali_title pb-2"><Unicons.UilBag size="30"/> Experience</h3>
						<Container>
							<Row className="">

							<Col xs={3} className="">

									<div className="q_line"></div>
									
									<span className="q_icon"><FaFileCode size={20} /></span>
									
									<div className="q_line2"></div>
									
								</Col>
								<Col xs={9}>
								<Card className="educ py-2" data-aos="slide-left">
									<Card.Title className="q_title">Junior Automation Developer</Card.Title>
									<Card.Text className="q_name">CGI Philippines</Card.Text>
									<p className="q_year"><Unicons.UilCalendarAlt size="15"/> 2021 - Present</p>
								</Card>
								</Col>

								<Col xs={3} className="">

									<div className="q_line"></div>
									
									<span className="q_icon"><BsFillHouseDoorFill size={20} /></span>
									
									<div className="q_line2"></div>
									
								</Col>
								<Col xs={9}>
								<Card className="educ py-2" data-aos="slide-left">
									<Card.Title className="q_title">Property Presenter</Card.Title>
									<Card.Text className="q_name">Megaeast Properties, Inc.</Card.Text>
										<p className="q_year"><Unicons.UilCalendarAlt size="15"/> 2021</p>
								</Card>
								</Col>

								<Col xs={3} className="">

									<div className="q_line"></div>
									
									<span  className="q_icon"><Unicons.UilBriefcase/></span>
									
									<div className="q_line2"></div>	
								</Col>
								<Col xs={9}>
								<Card className="educ py-2" data-aos="slide-left">
									<Card.Title className="q_title">Account Management Executive</Card.Title>
									<Card.Text className="q_name">We Move People and Things, Inc.</Card.Text>
							
										<p className="q_year"><Unicons.UilCalendarAlt size="15"/> 2021</p>
							</Card>
								</Col>

								<Col xs={3} className="">

									<div className="q_line"></div>
									
									<span  className="q_icon"><Unicons. UilStore/></span>
									
									<div className="q_line2"></div>	
								</Col>
								<Col xs={9}>
								<Card className="educ py-2" data-aos="slide-left">
									<Card.Title className="q_title">Store Manager</Card.Title>
									<Card.Text className="q_name">Golden ABC, Inc.
									</Card.Text>
							
										<p className="q_year"><Unicons.UilCalendarAlt size="15"/> 2019 - 2020</p>	
								</Card>
								</Col>

								<Col xs={3} className="">

									<div className="q_line"></div>
									
									<span className="q_icon"><Unicons. UilStore/></span>
									
									<div className="q_line2"></div>	
								</Col>
								<Col xs={9}>
								<Card className="educ py-2" data-aos="slide-left">
									<Card.Title className="q_title">Department Supervisor</Card.Title>
									<Card.Text className="q_name">Metro Retail Stores Group, Inc.
									</Card.Text>
							
										<p className="q_year"><Unicons.UilCalendarAlt size="15"/> 2018 - 2019</p>
								</Card>
								</Col>

								<Col xs={3} className="">

									<div className="q_line"></div>
									
									<span  className="q_icon"><Unicons. UilStore/></span>
									
									<div className="q_line2"></div>
									<span className="q_icon pl-5"><Unicons.UilRocket size="30"/></span>	
								</Col>
								<Col xs={9}>
								<Card className="educ py-2" data-aos="slide-left">
									<Card.Title className="q_title">Store Supervisor</Card.Title>
									<Card.Text className="q_name">Suyen Corporation</Card.Text>
							
										<p className="q_year"><Unicons.UilCalendarAlt size="15"/> 2017 - 2018</p>
										</Card>
								</Col>
							</Row>
						</Container>
					</Col>
					<Col md={6} className="e_sec">
				{/*Education*/}
						<h3 className="quali_title pb-2"><Unicons.UilGraduationCap  size="30"/> Education</h3>
						<Container>
							<Row>

								<Col xs={3} className="">

									<div className="q_line"></div>
									
									<span  className="q_icon"><Unicons.UilDesktopAlt/></span>
									
									<div className="q_line2"></div>
									
								</Col>
								<Col xs={9}>
								<Card className="educ py-2" data-aos="slide-left">
									<Card.Title className="q_title">Zuitt Short Courses</Card.Title>
									<p className="q_name">Amazon Web Services</p>
									<p className="q_name">PHP, MySQL, Laravel</p>
									<p className="q_year"><Unicons.UilCalendarAlt size="15"/> 2022</p>
								</Card>
								</Col>

								<Col xs={3} className="">

									<div className="q_line"></div>
									
									<span  className="q_icon"><Unicons.UilDesktopAlt/></span>
									
									<div className="q_line2"></div>
									
								</Col>
								<Col xs={9}>
								<Card className="educ py-2" data-aos="slide-left">
									<Card.Title className="q_title">Udemy</Card.Title>
									<p className="q_name">PHP for Beginners - Become a PHP Master</p>
									<p className="q_year"><Unicons.UilCalendarAlt size="15"/> 2022</p>
								</Card>
								</Col>

								<Col xs={3} className="">

									<div className="q_line"></div>
									
									<span  className="q_icon"><Unicons.UilDesktopAlt/></span>
									
									<div className="q_line2"></div>
									
								</Col>
								<Col xs={9}>
								<Card className="educ py-2" data-aos="slide-left">
									<Card.Title className="q_title">Zuitt Coding Bootcamp</Card.Title>
									<p className="q_name">Developer Career Program</p>
									<p className="q_year"><Unicons.UilCalendarAlt size="15"/> 2021</p>
								</Card>
								</Col>

								<Col xs={3} className="">

									<div className="q_line"></div>
									
									<span  className="q_icon"><Unicons.UilDesktopAlt/></span>
									
									<div className="q_line2"></div>
									
								</Col>
								<Col xs={9}>
								<Card className="educ py-2" data-aos="slide-left">
									<Card.Title className="q_title">Udemy</Card.Title>
									<p className="q_name">Build your own awesome responsive Personal PORTFOLIO site</p>
									<p className="q_name">The Complete 2021 Web Development Bootcamp</p>
									<p className="q_year"><Unicons.UilCalendarAlt size="15"/> 2021</p>
								</Card>
								</Col>

								<Col xs={3} className="">

									<div className="q_line"></div>
									
									<span  className="q_icon"><Unicons.UilBooks /></span>
									
									<div className="q_line3"></div>
									<span className="q_icon pl-5"><Unicons.UilRocket size="30"/></span>
									
								</Col>
								<Col xs={9}>
								<Card className="educ py-2" data-aos="slide-left">
									<Card.Title className="q_title">Our Lady of Fatima University</Card.Title>
									<p className="q_name">Bachelor of Science in Business Administration Major in Business Management</p>
									<p className="q_year"><Unicons.UilCalendarAlt size="15"/> 2014-2017</p>
									
								</Card>
								</Col>

							</Row>
						</Container>
					</Col>
				</Row>
			</Container>
		</Container>

		)
}