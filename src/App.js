import React from 'react';
import './App.css';
import { Container } from 'react-bootstrap';


import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import AboutMe from './pages/AboutMe';
import Skillset from './pages/Skillset';
import Qualification from './pages/Qualification';
import Projects from './pages/Projects';
import Contact from './pages/Contact';
import Footer from './components/Footer';

function App() {

  return (

    <>
      
      <AppNavbar/>
      <Home /> 
      <AboutMe /> 
      <Skillset />
      <Qualification />
      <Projects />
      <Contact />
      <Footer/>

      

    </>

    )

}

export default App;
